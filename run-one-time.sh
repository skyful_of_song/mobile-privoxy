echo "export ALL_PROXY=127.0.0.1:8118

alias block='bash ~/privoxy/block.sh'
alias allow='bash ~/privoxy/allow.sh'
alias caution='bash ~/privoxy/caution.sh'
alias primary='bash ~/privoxy/primary.sh'
alias secondary='bash ~/privoxy/secondary.sh'
" >> ~/.bashrc

echo {+block{Sneaky sites!}} > user.action
touch trust